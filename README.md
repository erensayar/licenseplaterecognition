# LicensePlateRecognition

License Plate Recognition with Python

## Requirements Packages:
- opencv-python
- pytesseract
- imutils

NOTE:
pytesseract module needs tesseract binaries you can install with this commands;
  - for Debian like systems
  ```sh
  sudo apt install tesseract-ocr
  sudo apt install libtesseract-dev
  ```
  - for MacOs
  ```sh
  brew tesseract
  ```

## Usage

- Create virtual environment
```sh
virtualenv venv -p python3
```
- Activate virtual environment

for linux
```sh
source venv/bin/activate
```
- Run the script

```sh
python run.py images/Araba2.jpg
```
