from os import path
import sys
import cv2


def pre_process_image(file_path):
    import numpy as np
    from imutils import grab_contours

    plate_area = None

    img = cv2.imread(file_path)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    filtered = cv2.bilateralFilter(gray_img, 6, 250, 250)
    edged = cv2.Canny(filtered, 30, 200)

    #Finding All Contour
    contours = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    sorted_contours = sorted(grab_contours(contours), key=cv2.contourArea, reverse=True)[:10]

    #Finding Corners Of Plate Contour
    for c in sorted_contours:
        epsilon = 0.06 * cv2.arcLength(c, True)  #NOTE: 0.06 better than 0.018.
        approx = cv2.approxPolyDP(c, epsilon, True)
        if len(approx) == 4:
            plate_area = approx
            break

    #Mask: Filling the image with black pixels excluding the "Plate" plane 
    mask = np.zeros(gray_img.shape, np.uint8)
    cv2.drawContours(mask, [plate_area], 0, (255, 255, 255), -1)
    new_img = cv2.bitwise_and(img, img, mask=mask)

    #Clipping: "Plate" Plane Clipping From Image
    #We have just plate area
    x, y = np.where(mask == 255)
    topx, topy = np.min(x), np.min(y)
    bottomx, bottomy = np.max(x), np.max(y)

    return gray_img[topx:bottomx + 1, topy:bottomy + 1]

def show_image(image):
    cv2.imshow("Plaka", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def predict_plate_number(image):
    from pytesseract import image_to_string

    return image_to_string(image, lang="eng")

def main():
    image_file_path=sys.argv[1]

    if path.isfile(image_file_path):
        processed_image = pre_process_image(image_file_path)
        show_image(processed_image)
        predicted_text = predict_plate_number(processed_image)
        print(predicted_text)
    else:
        print ("File not found")
        sys.exit(2)

if __name__ == '__main__':
    main()
